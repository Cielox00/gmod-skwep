ITEM.Name = 'Sonic Screwdriver'
ITEM.Price = 999999
ITEM.Model = 'models/weapons/w_sonicsd.mdl' --[[This is where you will set the skin model to replace with]]--
ITEM.SkinClass = "swep_sonicsd_skin" --[[Input the weapon "skinned" class here]]--
ITEM.WeaponClass = 'weapon_zm_carry' --[[The weapon entity to change skin]]--
ITEM.SingleUse = false --[[Should this skin be rented again on every spawn? If yes set this to true]]--
ITEM.Identifier = "SCrewdriver" --[[Set a unique name for each item, this MUST be set because of a bug that allows people to skin their items without buying it]]--

function ITEM:OnEquip(ply)
	PSS_PlaySkinEquipSound(ply)
	if table.HasValue(ply.psskintable, self.Identifier) then return end
	table.insert(ply.psskintable, self.Identifier)
end

function ITEM:OnHolster(ply)
	local num = table.KeyFromValue(ply.psskintable, self.Identifier)
	table.remove(ply.psskintable, num)
	if table.HasValue(PSS.Config.EssentialWeps, self.WeaponClass) then
		ply:Give(self.WeaponClass)
		ply:SelectWeapon(self.WeaponClass)
	end
	PSS_PlaySkinHolsterSound(ply)
end

function ITEM:OnBuy(ply)
	if PSS.Config.Notifications and PSS.Config.MessageBuy then
		if CLIENT then return end
		BroadcastLua('chat.AddText(Color(0, 255, 0), "[PSSkins] ", Color(255, 255, 255), "'..ply:Name()..' ", Color(255,255,255), "has bought skin ", Color(0, 255, 0), "'..self.Name..'")')
	end
	if not self.SingleUse then return end
	PSS_PlaySkinEquipSound(ply)
	table.insert(ply.psskintable, self.Identifier)
	ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
	PSS_ReskinWeapon(ply, self.SkinClass, self.WeaponClass, self.Identifier, self.Name)
end

function ITEM:Think(ply)
	if ply:Alive() and IsValid(ply) and not ply:IsSpec() then
		if CLIENT then return end
		if not table.HasValue(ply.psskintable, self.Identifier) then return end
		PSS_ReskinWeapon(ply, self.SkinClass, self.WeaponClass, self.Identifier, self.Name)
	end
end