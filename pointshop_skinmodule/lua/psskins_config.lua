PSS = {}
PSS.Config = {}

--[[Should notifications be enabled for players? If not set to false]]--
PSS.Config.Notifications 		= true
PSS.Config.MessageInitialize 	= true -- Message to confirm the script is running?
PSS.Config.MessageEquip 		= true -- Should the player be shown a notification that his skin is active?
PSS.Config.MessageBuy 			= true -- When a player buys a skin, should all the players on the server be notified?

--[[Should skinned weapons be able to be picked up when dropped? If yes set to true]]--
PSS.Config.PickupSkins 			= false

--[[Should the owner of the skinned weapon be allowed to pick it back up? if not set to false]]--
PSS.Config.OwnerPickupSkin 		= true

--[[Should sounds be played when eqiupping and holstering skin? If yes set to true, if not set to false]]--
PSS.Config.EnableSound 			= true
PSS.Config.SoundShopEquip		= true -- Play sound on buy?
PSS.Config.SoundShopHolster		= true -- Play sound on holster?
PSS.Config.SoundEquipWep		= true -- Play sound when given skin?

--[[Add lines with corresponding order number to add more skin equip sounds (Sounds are automatically added to download list if enabled)]]--
PSS.Config.EquipSounds = {}
PSS.Config.EquipSounds[1] = "pssskins/equipsound01.wav"
PSS.Config.EquipSounds[2] = "pssskins/equipsound02.wav"

--[[Add lines with corresponding order number to add more skin holster sounds (Sounds are automatically added to download list if enabled)]]--
PSS.Config.HolsterSounds = {}
PSS.Config.HolsterSounds[1] = "pssskins/holstersound01.wav"
PSS.Config.HolsterSounds[2] = "pssskins/holstersound02.wav"

--[[Add the weapons here that are given to each player on loadout on the server]]--
PSS.Config.EssentialWeps = {}
PSS.Config.EssentialWeps[1] = "weapon_zm_improvised"
PSS.Config.EssentialWeps[2] = "weapon_zm_carry"
PSS.Config.EssentialWeps[3] = "weapon_ttt_unarmed"