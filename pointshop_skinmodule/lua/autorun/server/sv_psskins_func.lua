include('psskins_config.lua')
PSS_RWepTable = {}
PSS_PDataTable = {}

function PSS_LoadUp()
	if PSS.Config.EnableSound then
		for i=1,#PSS.Config.EquipSounds do
			resource.AddFile( "sound/"..PSS.Config.EquipSounds[i] )
		end
		for i=1,#PSS.Config.HolsterSounds do
			resource.AddFile( "sound/"..PSS.Config.HolsterSounds[i] )
		end
	end
end

function PSS_SetUpPTables(ply)
	ply.psskintable = {}
	ply.PSS_EntTable = {}
	timer.Simple(5, function()
	if PSS.Config.Notifications and PSS.Config.MessageInitialize then
		ply:SendLua("chat.AddText(Color(0, 255, 0), '[PSSkins] ', Color(255, 255, 255), 'PS Skins Loaded!')")
	end
	end)
end
hook.Add("PlayerInitialSpawn","PSS_SetUpPTables",PSS_SetUpPTables)

function PSS_ReskinWeapon(ply, SkinClass, WeaponClass, Identifier, SkinName)
	if not table.HasValue(ply.psskintable, Identifier) then return end
	if not IsValid(ply:GetActiveWeapon()) then return end
	if ply:GetActiveWeapon():GetClass() == WeaponClass then
		ply:StripWeapon(WeaponClass)
		ply:Give(SkinClass)
		ply:SelectWeapon(SkinClass)
		table.insert(PSS_RWepTable, ply:GetActiveWeapon())
		table.insert(ply.PSS_EntTable, ply:GetActiveWeapon())
		if not table.HasValue(PSS_PDataTable, ply) then
			table.insert(PSS_PDataTable, ply)
		end
		if PSS.Config.Notifications and PSS.Config.MessageEquip then
			ply:SendLua('chat.AddText(Color(0, 255, 0), "[PSSkins] ", Color(255, 255, 255), "Your skin ", Color(0,255,0), "'..SkinName..' ", Color(255, 255, 255), "has been equipped.")')
			if PSS.Config.EnableSound and PSS.Config.SoundEquipWep then
				ply:SendLua('surface.PlaySound("'..table.Random(PSS.Config.EquipSounds)..'")')
			end
		end
	end
end

function PSS_PlaySkinEquipSound(ply)
	if not PSS.Config.EnableSound then return end
	if not PSS.Config.SoundShopEquip then return end
	ply:EmitSound(table.Random(PSS.Config.EquipSounds), 500, 100)
	ply:SendLua('surface.PlaySound("'..table.Random(PSS.Config.EquipSounds)..'")')
end

function PSS_PlaySkinHolsterSound(ply)
	if not PSS.Config.EnableSound then return end
	if not PSS.Config.SoundShopHolster then return end
	ply:EmitSound(table.Random(PSS.Config.HolsterSounds), 500, 100)
	ply:SendLua('surface.PlaySound("'..table.Random(PSS.Config.HolsterSounds)..'")')
end

function PSS_CanPickUp(ply, wep)
	if PSS.Config.OwnerPickupSkin then
		if table.HasValue(ply.PSS_EntTable, wep) then return true end
	end
	if not PSS.Config.PickupSkins then
		if table.HasValue(PSS_RWepTable, wep) then return false end
	end
end
hook.Add("PlayerCanPickupWeapon","PSS_CanPickUp",PSS_CanPickUp)

function PSS_ClearRTable()
	table.Empty(PSS_RWepTable)
	for i=1,#PSS_PDataTable do
		table.Empty(PSS_PDataTable[i].PSS_EntTable)
	end
	table.Empty(PSS_PDataTable)
end
hook.Add("TTTPrepareRound","PSS_ClearRTable",PSS_ClearRTable)

PSS_LoadUp()