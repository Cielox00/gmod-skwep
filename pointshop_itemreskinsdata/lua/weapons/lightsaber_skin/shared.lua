
if ( SERVER ) then

	AddCSLuaFile( "shared.lua" )
	
	
end

SWEP.HoldType			= "melee"

if ( CLIENT ) then

	SWEP.PrintName			= "Lightsaber"			
	SWEP.Author				= "XcoliahX"
	SWEP.Slot				= 0
	SWEP.SlotPos			= 0
	SWEP.IconLetter			= "u"
	SWEP.DrawCrosshair		= false
	SWEP.ViewModelFlip = false
	
     


end

-----------------------Main functions----------------------------
 
-- function SWEP:Reload() --To do when reloading
-- end 
 
function SWEP:JumpReset()
         self.JumpRefire = true
end

function SWEP:Think()
	if self.Owner:KeyDown(IN_JUMP) then
		if self.Owner:IsOnGround() then
			self.Owner:SetVelocity(Vector(0,0,500))
		end
	end
	
         //Jump think
         if self.InAirDmg == 1 then
            local trace = {}
	    trace.start = self.Owner:GetPos()
	    trace.endpos = self.Owner:GetPos() + (self.Owner:GetUp() * -10)
	    trace.filter = self.Owner
	
	    local tr2 = util.TraceLine(trace)

	    self.Owner:SetHealth(self.PrevHP)
	    
	    if tr2.Hit then
	       self.InAirDmg = 0
	       self.JumpRefire = false
               self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
               self.Weapon:SetNextPrimaryFire(CurTime() + 0.3)

	       if SERVER then self.Owner:EmitSound(Sound("player/pl_pain5.wav")) end
            else
             local ang = self.Owner:GetAimVector()
	     local spos = self.Owner:GetShootPos()

	     local trace = {}
	     trace.start = spos
	     trace.endpos = spos + (ang * 150)
	     trace.filter = self.Owner
	
	     local tr = util.TraceLine(trace)
	 
	     if tr.HitNonWorld and self.JumpRefire == true then
	        self.JumpRefire = false

                local bullet = {}
                bullet.Num=5
                bullet.Src = self.Owner:GetShootPos()
	        bullet.Dir= self.Owner:GetAimVector()
	        bullet.Spread = Vector(0.1,0.1,0.1)
	        bullet.Tracer = 0
	        bullet.Force = 500
	        bullet.Damage = 200
	        self.Owner:FireBullets(bullet)
	        self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

	        
	        timer.Simple(0.5,self.JumpReset,self)
	        
                if SERVER then self.Owner:EmitSound(Sound("npc/vort/claw_swing"..tostring(math.random(1,2))..".wav")) end
                if SERVER then self.Owner:EmitSound(Sound("ls/lightsaber_swing"..tostring(math.random(1,3))..".wav")) end
				
             end
            end
           end
end

function SWEP:Initialize()
util.PrecacheSound("physics/flesh/flesh_impact_bullet" .. math.random( 3, 5 ) .. ".wav")
util.PrecacheSound("weapons/iceaxe/iceaxe_swing1.wav")
self:SetWeaponHoldType("melee") 
end
 
function SWEP:PrimaryAttack()
self.Weapon:SetNextPrimaryFire(CurTime() + .50)

local trace = self.Owner:GetEyeTrace()

if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
	bullet = {}
	bullet.Num    = 1
	bullet.Src    = self.Owner:GetShootPos()
	bullet.Dir    = self.Owner:GetAimVector()
	bullet.Spread = Vector(0, 0, 0)
	bullet.Tracer = 0
	bullet.Force  = 0
	bullet.Damage = 1000
self.Owner:FireBullets(bullet)
self.Owner:SetAnimation( PLAYER_ATTACK1 );
self.Weapon:EmitSound("ls/lightsaber_swing" .. math.random( 3, 5 ) .. ".wav")
else
	self.Weapon:EmitSound("ls/ls_throw")
	self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
end

end

/*---------------------------------------------------------
		SecondaryAttack
---------------------------------------------------------*/
SWEP.InAirDmg = 0
function SWEP:SecondaryAttack()
         if SERVER then self.Owner:EmitSound(Sound("ls/lightsaber_swing"..tostring(math.random(2,3))..".wav")) end
         if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
         
         self.Weapon:SetNextSecondaryFire(CurTime() + 100)
         --self.Weapon:SetNextPrimaryFire(CurTime() + 100)

         self.PrevHP = self.Owner:Health()
         
         if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * 400 + Vector(0,0,400)) end
         
         timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

function SWEP:SecondaryAttackDelay()
         --self.InAirDmg = 1
         --self.JumpRefire = true
end




-------------------------------------------------------------------

------------General Swep Info---------------
SWEP.Author   = "XcoliahX"
SWEP.Contact        = ""
SWEP.Purpose        = ""
SWEP.Instructions   = ""
SWEP.Spawnable      = false
SWEP.AdminSpawnable  = true
SWEP.Base				= "weapon_tttbase"
SWEP.DrawAmmo = false
-----------------------------------------------

------------Models---------------------------
SWEP.ViewModel      = "models/weapons/v_crewbar.mdl"
SWEP.WorldModel   = "models/weapons/v_crewbar.mdl"
-----------------------------------------------

-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 1000	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.Damage			= 5000	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 0 	--Bullet Spread
SWEP.Primary.ClipSize		= 0	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= 0	--Number of shots in next clip
SWEP.Primary.Automatic   	= false	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "none"	--Ammo Type
-------------End Primary Fire Attributes------------------------------------
 
-------------Secondary Fire Attributes-------------------------------------
SWEP.Secondary.Delay		= 120
SWEP.Secondary.Recoil		= 0
SWEP.Secondary.Damage		= 0
SWEP.Secondary.NumShots		= 1
SWEP.Secondary.Cone			= 0
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic   	= true
SWEP.Secondary.Ammo         = "none"
-------------End Secondary Fire Attributes--------------------------------
function SWEP:Deploy()
	self.Weapon:EmitSound( "weapons/knife/knife_deploy1.wav" ) --Plays a nifty sound when you pull it out.
end
